﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Coordinate
{
    public int i, j, k;
    public Coordinate(int i, int j, int k)
    {
        this.i = i;
        this.j = j;
        this.k = k;
    }

    public Coordinate(int i, int j)
    {
        this.i = i;
        this.j = j;
        this.k = 0;
    }

    public override string ToString()
    {
        return string.Format("Coordinate({0},{1},{2})", i, j, k);
    }

    public override bool Equals(object obj)
    {
        if (obj != null && obj is Coordinate)
        {
            var other = (Coordinate)obj;
            return other.i == i &&
                other.j == j &&
                other.k == k;

        }
        return false;
    }
    public override int GetHashCode()
    {
        return new Vector3(i, j, k).GetHashCode();
    }
}
