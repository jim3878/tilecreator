﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public class GridMapCreator : EditorWindow
{
    public enum ShowModeEnum
    {
        NONE,
        GridMap,
        MeshMap,
    }
    public string[] showMode;
    public string[] ShowMode
    {
        get
        {
            if (showMode == null || showMode.Length == 0)
            {
                List<String> sm = new List<string>();
                foreach (var mode in Enum.GetValues(typeof(ShowModeEnum)))
                {
                    sm.Add(mode.ToString());
                }
                showMode = sm.ToArray();
            }
            return showMode;
        }
    }
    public static ShowModeEnum currShowMode = 0;
    public enum DrawStateEnum
    {
        NONE,
        DRAW,
        CLEAR
    }
    private string[] drawState;
    public String[] DrawState
    {
        get
        {
            if (drawState == null || drawState.Length == 0)
            {
                List<string> s = new List<string>();
                foreach (var e in Enum.GetValues(typeof(DrawStateEnum)))
                {
                    s.Add(e.ToString());
                }
                drawState = s.ToArray();
            }
            return drawState;
        }
    }
    public static DrawStateEnum currDrawState = 0;

    public enum DrawShapeEnum
    {
        POINT,
        RECT,
    }
    public static Vector2 gridSize = Vector2.one;
    static MapData map;
    public static MapData Map
    {
        get
        {
            return map;
        }
    }
    static MapTable table;
    public static MapTable MapTable
    {
        get
        {
            return table;
        }
    }
    public static bool showGrid;
    public static bool showGridId;

    static string[] mapList;
    static string[] gridIDList;

    public static string mapPath = @"Assets/NavMesh2D/";
    public static string fileName = "MapTable";

    public static bool isEnable;
    public static bool drawSketch = false;
    public static List<Coordinate> mousePath = new List<Coordinate>();
    public static DrawShapeEnum drawShape = DrawShapeEnum.RECT;


    [MenuItem("Tools/格狀尋路圖產生器")]
    public static void MapTool()
    {
        EditorWindow.GetWindow(typeof(GridMapCreator));
    }

    private void OnEnable()
    {
        isEnable = true;
        table = LoadMapTable();
        showGrid = true;
        SceneView.onSceneGUIDelegate += OnMousueClick;
        if (table.mapList.Count == 0)
        {
            AddMap(0);
            LoadMap(0);
        }
        else if (table.mapList.Count <= table.currentIndex)
        {
            LoadMap(0);
        }
        else
        {
            LoadMap(table.currentIndex);
        }

        UpdateGridDataPopup();
        UpdateMapPopup();
        InitGizmosTileMesh();

    }

    private void OnDestroy()
    {
        isEnable = false;
    }


    static int selectMapIndex = 0;
    static int currentGridIDIndex = 0;
    private void OnGUI()
    {
        //地圖列表
        EditorGUI.BeginChangeCheck();
        selectMapIndex = EditorGUILayout.Popup("map List", selectMapIndex, mapList);
        if (EditorGUI.EndChangeCheck())
        {
            if (selectMapIndex == table.mapList.Count)
            {
                Debug.Log("new map");
                int index = table.mapList.Count;
                AddMap(index);
                LoadMap(index);
            }
            else
            {
                LoadMap(selectMapIndex);
                //map = table.map[selectMapIndex - 1];
            }
            SceneView.RepaintAll();
        }

        //地圖名稱
        EditorGUI.BeginChangeCheck();
        map.name = EditorGUILayout.TextField("map name", map.name);
        if (EditorGUI.EndChangeCheck())
        {
            UpdateMapPopup();
        }

        //格子列表
        EditorGUI.BeginChangeCheck();
        currentGridIDIndex = EditorGUILayout.Popup("grid ID", currentGridIDIndex, gridIDList);
        if (EditorGUI.EndChangeCheck())
        {
            if (currentGridIDIndex == gridIDList.Length - 1)
            {
                AddGridID(gridIDList.Length - 1);
            }
        }

        EditorGUI.BeginChangeCheck();
        table.gridDataList[currentGridIDIndex].name = EditorGUILayout.TextField("grid name", table.gridDataList[currentGridIDIndex].name);
        table.gridDataList[currentGridIDIndex].color = EditorGUILayout.ColorField("grid gizmos color", table.gridDataList[currentGridIDIndex].color);
        if (EditorGUI.EndChangeCheck())
        {
            UpdateGridDataPopup();
            SceneView.RepaintAll();
        }

        EditorGUI.BeginChangeCheck();
        currShowMode = (ShowModeEnum)GUILayout.Toolbar((int)currShowMode, ShowMode);

        if (currShowMode == ShowModeEnum.GridMap)
        {
            currDrawState = (DrawStateEnum)GUILayout.Toolbar((int)currDrawState, DrawState);
            drawShape = (DrawShapeEnum)EditorGUILayout.EnumPopup("Draw Shape", drawShape);
        }
        if (EditorGUI.EndChangeCheck())
        {
            InitGizmosTileMesh();
        }

        EditorGUI.BeginChangeCheck();
        showGrid = EditorGUILayout.Toggle("顯示方格", showGrid);
        showGridId = EditorGUILayout.Toggle("顯示方格ID", showGridId);

        if (EditorGUI.EndChangeCheck())
        {
            SceneView.RepaintAll();
        }

        EditorGUI.BeginChangeCheck();
        gridSize = EditorGUILayout.Vector2Field(label: "方格尺寸 ", value: gridSize);
        if (EditorGUI.EndChangeCheck())
        {
            map.gridMap.GridSize = gridSize;
            SaveMapTable(map);
            InitGizmosTileMesh();
        }

        if (GUILayout.Button("Bake"))
        {
            map.meshMapList = MeshMap.Creat(map.gridMap, table.gridDataList);
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Save"))
        {
            SaveMapTable(map);
        }
    }

    private void AddGridID(int id)
    {
        //InputInt.onClose -= AddGridID;
        Debug.Log("add id : " + id);

        var gridData = new GridData(id, new Color(0, 1, 0, 0.2f));
        table.gridDataList.Add(gridData);
        //map.gridMap.GridIDList.Sort();
        UpdateGridDataPopup();
        currentGridIDIndex = id;
        this.Repaint();

    }

    private Vector3 CoordinateToPosition(Coordinate coordinate)
    {
        return new Vector3(coordinate.i * gridSize.x, coordinate.j * gridSize.y);
    }

    private Coordinate PositionToCoordinate(Vector3 Pos)
    {
        float x = (Pos.x / gridSize.x);
        float y = (Pos.y / gridSize.y);
        int mX, mY;
        mX = Mathf.CeilToInt(x);
        mY = Mathf.CeilToInt(y);


        Coordinate result = new Coordinate(mX, mY);
        //Debug.Log(result);
        return result;
    }

    private Vector3 GetMousuPosition()
    {
        Vector2 mousePos = Event.current.mousePosition;
        mousePos.y = SceneView.currentDrawingSceneView.camera.pixelHeight - mousePos.y;
        return SceneView.currentDrawingSceneView.camera.ScreenPointToRay(mousePos).origin;
    }

    //讀檔
    private MapTable LoadMapTable()
    {
        if (!Directory.Exists(mapPath))
        {
            Directory.CreateDirectory(mapPath);
        }
        MapTable table;
        if (!File.Exists(mapPath + fileName + ".asset"))
        {
            table = ScriptableObject.CreateInstance<MapTable>();
            table.mapList = new List<MapData>();
            AssetDatabase.CreateAsset(table, mapPath + fileName + ".asset");
            Debug.Log("[MapCreator] creat new assets");
        }
        else
        {
            table = AssetDatabase.LoadAssetAtPath<MapTable>(mapPath + fileName + ".asset");
        }
        return table;
    }

    //存檔
    private void SaveMapTable(MapData map)
    {
        int index = table.mapList.FindIndex(x => x.ID == map.ID);
        if (index > -1)
        {
            //table.map.RemoveAt(index);
            table.mapList[index] = map;
        }
        else
        {
            table.mapList.Add(map);
        }
        table.UpdateGridDataOfMesh();
        EditorUtility.SetDirty(table);
        Debug.Log("[MapCreator]SaveData");
        UpdateMapPopup();
    }

    //更新地圖列表
    private void UpdateMapPopup()
    {
        List<string> result = new List<string>();
        foreach (var map in table.mapList)
        {
            result.Add(string.Format("[{0}]{1}", map.ID, map.name));
        }
        result.Add("add new map...");


        mapList = result.ToArray();
        InitGizmosTileMesh();
        SceneView.RepaintAll();
    }

    private void UpdateGridDataPopup()
    {
        var result = new List<string>();
        foreach (var data in table.gridDataList)
        {
            result.Add(string.Format("[{0}]{1}", data.ID, data.name));
        }
        result.Add("Add new..");

        gridIDList = result.ToArray();
    }



    void OnMousueClick(SceneView aView)
    {
        if (!isEnable)
            return;
        var sceneEvent = Event.current;

        switch (drawShape)
        {
            case DrawShapeEnum.POINT:
                DrawPoint(sceneEvent);
                break;
            case DrawShapeEnum.RECT:
                DrawRectGridTile(sceneEvent);
                break;
        }
    }

    Coordinate startCoord;
    void DrawRectGridTile(Event sceneEvent)
    {
        if (!isEnable || currShowMode != ShowModeEnum.GridMap || currDrawState == DrawStateEnum.NONE)
            return;
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        if (sceneEvent.type == EventType.MouseDown && sceneEvent.button == 0)
        {
            //起點
            drawSketch = true;

            mousePath.Clear();
            var mousePos = GetMousuPosition();
            startCoord = PositionToCoordinate(mousePos);
            var startGrid = new GridTile(startCoord, -1);
            DrawRect_leftUp = startGrid;
            DrawRect_rightDown = startGrid;

            SceneView.RepaintAll();
        }
        else if (sceneEvent.type == EventType.MouseDrag && sceneEvent.button == 0)
        {
            var mousePos = GetMousuPosition();
            var coord = PositionToCoordinate(mousePos);

            var currentGrid = new GridTile(coord, -1);
            DrawRect_rightDown = currentGrid;

            SceneView.RepaintAll();
        }
        else if ((sceneEvent.type == EventType.MouseUp || sceneEvent.type == EventType.MouseLeaveWindow) && drawSketch && sceneEvent.button == 0)
        {
            drawSketch = false;
            var mousePos = GetMousuPosition();
            var endCoord = PositionToCoordinate(mousePos);

            int startX = Mathf.Min(startCoord.i, endCoord.i);
            int endX = Mathf.Max(startCoord.i, endCoord.i);
            int startY = Mathf.Min(startCoord.j, endCoord.j);
            int endY = Mathf.Max(startCoord.j, endCoord.j);
            for (int i = startX; i <= endX; i++)
            {
                for (int j = startY; j <= endY; j++)
                {
                    switch (currDrawState)
                    {
                        case DrawStateEnum.DRAW://pan
                            PaintTile(new Coordinate(i, j));
                            break;
                        case DrawStateEnum.CLEAR://eraser
                            ClearTile(new Coordinate(i, j));
                            break;
                    }
                }
            }
        }
    }

    void DrawPoint(Event sceneEvent)
    {
        if (currShowMode != ShowModeEnum.GridMap)
            return;
        Coordinate coord = default(Coordinate);
        if (sceneEvent.type == EventType.MouseDown && sceneEvent.button == 0)
        {
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            mousePath.Clear();
            var mousePos = GetMousuPosition();
            coord = PositionToCoordinate(mousePos);
            mousePath.Add(coord);


            SceneView.RepaintAll();
        }
        else if (sceneEvent.type == EventType.MouseDrag && sceneEvent.button == 0)
        {
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            var mousePos = GetMousuPosition();
            coord = PositionToCoordinate(mousePos);
            if (!mousePath.Contains(coord))
            {
                mousePath.Add(coord);
                PaintTile(coord);
            }

            SceneView.RepaintAll();
        }
        else
        {
            return;
        }
        switch (currDrawState)
        {
            case DrawStateEnum.DRAW://pan
                PaintTile(coord);
                break;
            case DrawStateEnum.CLEAR://eraser
                ClearTile(coord);
                break;
        }
    }

    private void PaintTile(Coordinate coordinate)
    {
        int index = map.gridMap.TileList.FindIndex(x => x.coordinate.Equals(coordinate));
        if (index > -1)
        {
            map.gridMap.TileList.RemoveAt(index);
        }
        var tile = new GridTile(coordinate, currentGridIDIndex);
        map.gridMap.AddTile(tile);
        
        AddGizmosTile(tile);
        SceneView.RepaintAll();
    }

    private void ClearTile(Coordinate coordinate)
    {
        int index = map.gridMap.TileList.FindIndex(x => x.coordinate.Equals(coordinate));
        if (index > -1)
        {
            map.gridMap.TileList.RemoveAt(index);
            RemoveGizmosTile(coordinate);
        }
        SceneView.RepaintAll();
    }

    [DrawGizmo(GizmoType.NotInSelectionHierarchy)]
    static void OnGizmosDraw(Transform objectTransform, GizmoType gizmoType)
    {
        GizmosDrawGridLine();

        switch (currShowMode)
        {
            case ShowModeEnum.GridMap:
                GizmosDrawGridTile();
                GismosDrawSketchRect();
                break;
            case ShowModeEnum.MeshMap:
                GismozDrawMesh();
                break;
            default:
                break;
        }

        //
    }

    static Dictionary<Coordinate, GizmosTileData> gizmosTileMesh;
    static void InitGizmosTileMesh()
    {
        gizmosTileMesh = new Dictionary<Coordinate, GizmosTileData>();
        foreach (var tile in map.gridMap.TileList)
        {
            AddGizmosTile(tile);
        }
    }

    static void AddGizmosTile(GridTile tile)
    {
        Mesh m = new Mesh();
        m.SetVertices(tile.Vertices);
        m.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
        m.RecalculateNormals();

        if (gizmosTileMesh.ContainsKey(tile.coordinate))
        {
            gizmosTileMesh.Remove(tile.coordinate);
        }
        gizmosTileMesh.Add(tile.coordinate, new GizmosTileData(tile, m));
    }

    static void RemoveGizmosTile(Coordinate coordinate)
    {
        gizmosTileMesh.Remove(coordinate);
    }


    static void GizmosDrawGridTile()
    {
        //Debug.Log("draw");

        if (!isEnable)
            return;
        foreach (var data in gizmosTileMesh)
        {
            var grid = data.Value.tile;
            var mesh = data.Value.mesh;
            Gizmos.color = table.gridDataList[grid.ID].color;

            Gizmos.DrawMesh(mesh);
            //Gizmos.color = Color.black;
            if (showGridId)
            {
                GUIStyle style = new GUIStyle();
                style.alignment = TextAnchor.MiddleCenter;
                style.normal.textColor = Color.black;
                Handles.Label((grid.Vertices[0] + grid.Vertices[1]) / 2, grid.ID.ToString(), style);
            }

        }
    }

    static void GismozDrawMesh()
    {
        if (map.meshMapList == null)
            return;

        foreach (var mesh in map.meshMapList)
        {
            foreach (var t in mesh.meshList)
            {
                Gizmos.color = table.gridDataList[t.GridIndex].color;
                var m = new Mesh();
                m.SetVertices(t.Vertices);
                m.triangles = new int[] { 0, 1, 2 };
                m.RecalculateNormals();
                Gizmos.DrawMesh(m);

                Gizmos.color = Color.black;
                foreach (var e in t.Vectors)
                {
                    Gizmos.DrawLine(e.from, e.to);
                }

                foreach (var v in t.Vertices)
                {
                    Gizmos.DrawCube(v, Vector3.one * (0.1f));
                }
            }
        }
    }

    static GridTile DrawRect_leftUp;
    static GridTile DrawRect_rightDown;
    static Color DrawRect_color = new Color(0, 0, 1, 0.05f);
    static void GismosDrawSketchRect()
    {
        if (drawSketch && currDrawState != DrawStateEnum.NONE && currShowMode == ShowModeEnum.GridMap)
        {
            Mesh m = new Mesh();
            List<Vector3> gridPoint = new List<Vector3>();
            DrawRect_leftUp.SetGridSize(gridSize);
            DrawRect_rightDown.SetGridSize(gridSize);
            float stX = Mathf.Min(DrawRect_rightDown.GetPosition.x, DrawRect_leftUp.GetPosition.x) - gridSize.x / 2;
            float stY = Mathf.Max(DrawRect_leftUp.GetPosition.y, DrawRect_rightDown.GetPosition.y) + gridSize.y / 2;
            float edX = Mathf.Max(DrawRect_rightDown.GetPosition.x, DrawRect_leftUp.GetPosition.x) + gridSize.x / 2;
            float edY = Mathf.Min(DrawRect_leftUp.GetPosition.y, DrawRect_rightDown.GetPosition.y) - gridSize.y / 2;

            gridPoint.Add(new Vector3(stX, stY));
            gridPoint.Add(new Vector3(edX, stY));
            gridPoint.Add(new Vector3(edX, edY));
            gridPoint.Add(new Vector3(stX, edY));


            m.SetVertices(gridPoint);
            m.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
            m.RecalculateNormals();

            Gizmos.color = DrawRect_color;
            Gizmos.DrawMesh(m);
        }
    }

    static void GizmosDrawGridLine()
    {
        if (isEnable && showGrid && gridSize.x > 0 && gridSize.y > 0)
        {
            Gizmos.color = Color.white;
            Vector3 minGrid = Camera.current.ScreenPointToRay(new Vector2(0f, 0f)).origin;
            Vector3 maxGrid = Camera.current.ScreenPointToRay(new Vector2(Camera.current.pixelWidth, Camera.current.pixelHeight)).origin;
            for (float i = Mathf.Round(minGrid.x / gridSize.x) * gridSize.x; i < Mathf.Ceil(maxGrid.x / gridSize.x) * gridSize.x && gridSize.x > 0.05f; i += gridSize.x)
                Gizmos.DrawLine(new Vector3(i, minGrid.y, 0.0f), new Vector3(i, maxGrid.y, 0.0f));
            for (float j = Mathf.Round(minGrid.y / gridSize.y) * gridSize.y; j < Mathf.Ceil(maxGrid.y / gridSize.y) * gridSize.y && gridSize.y > 0.05f; j += gridSize.y)
                Gizmos.DrawLine(new Vector3(minGrid.x, j, 0.0f), new Vector3(maxGrid.x, j, 0.0f));
        }
    }

    void AddMap(int id)
    {
        map = new MapData(id);

        table.mapList.Add(map);
        UpdateMapPopup();
    }

    void LoadMap(int id)
    {
        if (table == null)
        {
            LoadMapTable();
        }
        table.currentIndex = id;
        map = table.mapList[id];
        selectMapIndex = id;
        gridSize = map.gridMap.GridSize;
        InitGizmosTileMesh();
    }

    public class ErrorWindows : EditorWindow
    {
        public static Action onClose;
        public static void Open(string description)
        {
            var w = EditorWindow.GetWindow<ErrorWindows>(typeof(ErrorWindows));
            w.position = new Rect(new Vector2(400, 300), new Vector2(200, 150));

            w.description = description;
        }

        private string description;

        private void OnGUI()
        {
            EditorGUILayout.LabelField(description);
            if (GUILayout.Button("OK"))
            {
                this.Close();
            }
        }
        private void OnDestroy()
        {
            if (onClose != null)
            {
                onClose();
            }
        }
    }

    public class InputInt : EditorWindow
    {
        public static Action<int> onClose;
        public static void Open(string descrietion, int defaultValue = 0)
        {
            var w = EditorWindow.GetWindow<InputInt>(typeof(InputInt));
            w.position = new Rect(new Vector2(400, 300), new Vector2(200, 150));
            w.description = descrietion;
            w.value = defaultValue;
        }
        private string description = "enter int";
        private int value;

        private void OnGUI()
        {
            EditorGUILayout.LabelField(description);
            value = EditorGUILayout.IntField(value);
            if (GUILayout.Button("OK"))
            {
                this.Close();
            }

        }
        private void OnDestroy()
        {
            if (onClose != null)
            {
                onClose(value);
            }
        }
    }

    public struct GizmosTileData
    {
        public GridTile tile;
        public Mesh mesh;
        public GizmosTileData(GridTile tile, Mesh mesh)
        {
            this.tile = tile;
            this.mesh = mesh;
        }
    }
}

