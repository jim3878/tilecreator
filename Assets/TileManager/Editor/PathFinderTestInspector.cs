﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(pathFinderTest))]
public class PathFinderTestInspector : Editor{

    pathFinderTest pft;
    
    private void OnEnable()
    {
        pft = target as pathFinderTest;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("find"))
        {
            PathFinder pf = new PathFinder(GridMapCreator.Map.meshMapList);
            var way= pf.GetPath(pft.player.position, pft.target.position);

            pft.wayV = way;
            pft.st = way[0].from;
            pft.ed = way[0].to;
        }
    }
}
