﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crossTest : MonoBehaviour {

    public Transform a1, a2, b1, b2;

    private void OnDrawGizmos()
    {
        Vector a = new Vector(a1.position, a2.position);
        Vector b = new Vector(b1.position, b2.position);
        Gizmos.color = Color.black;
        Gizmos.DrawLine(a.from, a.to);
        Gizmos.DrawLine(b.from, b.to);
        if (a.IsCross(b))
        {
            Gizmos.DrawCube(a.GetCrossPoint(b),Vector3.one*0.5f);
        }    
    }
}
