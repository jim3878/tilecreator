﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VectorTest : MonoBehaviour
{

    public Text result;
    public Transform a1, a2;
    public Transform b1, b2;

    private void OnDrawGizmos()
    {
        Triangle t = new Triangle(a1.position, a2.position, b1.position);

        result.text = t.IsPointIn(b2.position).ToString();
        //var a = new Vector(a1.position, a2.position);
        //var b = new Vector(b1.position, b2.position);
        //Gizmos.color = Color.blue;
        //Gizmos.DrawLine(a.from, a.to);
        //Gizmos.DrawLine(b.from, b.to);

        ////result.text = a.IsPointTouch(b.from).ToString();
        //result.text = a.IsCross(b).ToString();

        //MeshBaker mb = new MeshBaker();
        //var c = mb.GetCircleBox(a1.position, a2.position, b1.position);

        //Gizmos.DrawSphere(c.center, c.radius);
    }
}
