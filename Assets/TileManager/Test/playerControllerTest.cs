﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerControllerTest : MonoBehaviour
{

    enum StateEnum
    {
        WALK,
        IDLE
    }


    public MapTable table;
    public Transform player;
    public bool test;
    public Vector3 startPosition;
    public Vector3 testPosition;
    MapData map;
    PathFinder pf;
    List<WayVector> wayList;
    StateEnum state;

    // Use this for initialization
    void Start()
    {
        state = StateEnum.IDLE;
        map = table.mapList[0];
        pf = new PathFinder(map.meshMapList);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
#if MAP_DEBUG
            Debug.Log("*************New path*************");
#endif
            Vector3 mousePosition;
            if (!test)
            {
                mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                testPosition = mousePosition;
                startPosition = player.position;
            }
            else
            {
                transform.position = startPosition;
                mousePosition = testPosition;
            }
#if MAP_DEBUG
            Debug.Log(string.Format("player  from {0}  to{1}", player.position, mousePosition));
#endif
            mousePosition.z = player.transform.position.z;
            wayList = pf.GetPath(player.position, mousePosition);
            state = StateEnum.WALK;
            //Debug.Log("Get path");
            foreach (var v in wayList)
            {
                //  Debug.Log(v);
            }
        }

        if (state == StateEnum.WALK)
        {

            var distance = Time.deltaTime * 2;

            while (distance > 0 && wayList.Count > 0)
            {
                if ((player.position - wayList[0].to).magnitude <= distance)
                {
                    distance -= (player.position - wayList[0].to).magnitude;
                    player.position = wayList[0].to;
                    wayList.RemoveAt(0);
                }
                else
                {
                    player.position = Vector3.MoveTowards(player.position, wayList[0].to, distance);
                    distance = 0;
                }
            }

            if (wayList.Count == 0)
            {
                state = StateEnum.IDLE;
            }

        }
    }
}

