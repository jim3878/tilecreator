﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleMeshTest : MonoBehaviour
{

    public Transform p0, p1, p2, t;
    public float distance;
    public bool isInMesh;

    private void OnDrawGizmos()
    {
        var triangle = new TriangleMesh(new Triangle(p0.position, p1.position, p2.position), 0);
        Gizmos.DrawLine(p0.position, p1.position);
        Gizmos.DrawLine(p1.position, p2.position);
        Gizmos.DrawLine(p2.position, p0.position);
        Vector3 closepoint;
        isInMesh = triangle.IsPointIn(t.position);
        distance = triangle.Distance(t.position, out closepoint);

        Gizmos.DrawLine(t.position, closepoint);
    }
}
