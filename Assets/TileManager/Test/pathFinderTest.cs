﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathFinderTest : MonoBehaviour
{
    
    public Vector3 st = Vector3.zero;
    public Vector3 ed = Vector3.zero;

    public List<WayVector> wayV = null;

    public Transform player, target;

    private void OnDrawGizmos()
    {
        if (wayV == null)
            return;

        Gizmos.color = Color.black;
        foreach(var way in wayV)
        {
            Gizmos.DrawCube(way.from, Vector3.one * 0.3f);
            Gizmos.DrawLine(way.from, way.to);
        }
        
    }
}
