﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornerPointFinder
{
    Vector3 left, right;
    List<Vector3> cornerList;
    bool isFound;
    int currentIndex;



    public List<WayVector> Find(Vector3 start, Vector3 end, List<IMesh> meshPath)
    {
        List<WayVector> way = new List<WayVector>();
#if MAP_DEBUG
        Debug.Log("+mesh Count " + meshPath.Count);
#endif
        int prevCornerMeshIndex = 0;
        int lastLeftCornerMeshIndex = 1;
        int lastRightCornerMeshIndex = 1;
        Vector currLeftVector, currRightVector;
        var currMesh = meshPath[0];
        var nextMesh = meshPath[1];

        var currAdjacentEdge = currMesh.GetAdjecantEdge(nextMesh);
        Vector leftVector = new Vector(start, currAdjacentEdge.from);
        Vector rightVector = new Vector(start, currAdjacentEdge.to);

        for (int i = 1; i < meshPath.Count; i++)
        {
            if (i == meshPath.Count - 1)
            {
                currLeftVector = new Vector(leftVector.from, end);
                currRightVector = new Vector(leftVector.from, end);
            }
            else
            {
                currMesh = meshPath[i];
                nextMesh = meshPath[i + 1];
                currAdjacentEdge = currMesh.GetAdjecantEdge(nextMesh);


                currLeftVector = new Vector(leftVector.from, currAdjacentEdge.from);
                currRightVector = new Vector(leftVector.from, currAdjacentEdge.to);
            }
            //Debug.Log("new edge " + currAdjacentEdge);

            //檢查是否有新節點
            if (Vector3.Cross(currRightVector.Direction, rightVector.Direction).z >= 0 && Vector3.Cross(currLeftVector.Direction, rightVector.Direction).z >= 0)
            {
                //Debug.Log("new right corner " + rightVector.to);
                //若新左右向量都在原右向量的順時鐘方向
                //加入新路徑
#if MAP_DEBUG
                Debug.Log("create path on mesh " + i);
#endif
                way.AddRange(CreatNewWay(meshPath, prevCornerMeshIndex, lastRightCornerMeshIndex, rightVector));

                //原右向量為新拐角點
                prevCornerMeshIndex = lastRightCornerMeshIndex + 1;

                //新的最終向量由當前拐角點的下個鄰邊構成

                i = prevCornerMeshIndex + 1;

                if (i < meshPath.Count - 1)
                {
                    lastLeftCornerMeshIndex = prevCornerMeshIndex + 1;
                    lastRightCornerMeshIndex = prevCornerMeshIndex + 1;
                    currMesh = meshPath[i];
                    nextMesh = meshPath[i + 1];
                    currAdjacentEdge = currMesh.GetAdjecantEdge(nextMesh);

                    var corner = rightVector.to;
                    leftVector = new Vector(corner, currAdjacentEdge.from);
                    rightVector = new Vector(corner, currAdjacentEdge.to);
                }
                else
                {
                    lastLeftCornerMeshIndex = i;
                    lastRightCornerMeshIndex = i;
                    var corner = rightVector.to;
                    leftVector = new Vector(corner, currLeftVector.to);
                    rightVector = new Vector(corner, currRightVector.to);
                }
                continue;
            }
            else if (Vector3.Cross(currLeftVector.Direction, leftVector.Direction).z <= 0 && Vector3.Cross(currRightVector.Direction, leftVector.Direction).z <= 0)
            {
                //Debug.Log("new left corner " + leftVector.to);
                //若新左右向量都在原左向量的逆時鐘方向
                //加入新路徑
#if MAP_DEBUG
                Debug.Log("+create path on mesh " + i);
#endif
                way.AddRange(CreatNewWay(meshPath, prevCornerMeshIndex, lastLeftCornerMeshIndex, leftVector));

                //原左向量為新拐角點
                prevCornerMeshIndex = lastLeftCornerMeshIndex + 1;

                //新的最終向量由當前拐角點的下個鄰邊構成
                i = prevCornerMeshIndex + 1;

                if (i < meshPath.Count - 1)
                {
                    lastLeftCornerMeshIndex = prevCornerMeshIndex + 1;
                    lastRightCornerMeshIndex = prevCornerMeshIndex + 1;
                    currMesh = meshPath[i];
                    nextMesh = meshPath[i + 1];
                    currAdjacentEdge = currMesh.GetAdjecantEdge(nextMesh);

                    var corner = leftVector.to;
                    leftVector = new Vector(corner, currAdjacentEdge.from);
                    rightVector = new Vector(corner, currAdjacentEdge.to);
                }
                else
                {
                    lastLeftCornerMeshIndex = i;
                    lastRightCornerMeshIndex = i;
                    var corner = leftVector.to;
                    leftVector = new Vector(corner, currLeftVector.to);
                    rightVector = new Vector(corner, currRightVector.to);
                }
                continue;
            }

            //檢查新的左向量
            if (Vector3.Cross(currLeftVector.Direction, leftVector.Direction).z >= 0)
            {
                //Debug.Log("new left vector " + currLeftVector);
                //若在原左向順時鐘方向則成為新的左向量
#if MAP_DEBUG
                Debug.Log(string.Format("+Set new left vector v0 {0} v1{1}\n  left mesh index from {2} to {3} ", leftVector, currLeftVector, lastLeftCornerMeshIndex, i));
#endif
                lastLeftCornerMeshIndex = i;
                leftVector = currLeftVector;
            }

            //檢查新的右向量
            if (Vector3.Cross(currRightVector.Direction, rightVector.Direction).z <= 0)
            {
                //Debug.Log("new right vector " + currRightVector);
                //若在原右向量的逆時鐘方向則成為新的右向量
#if MAP_DEBUG
                Debug.Log(string.Format("+Set new right vector v0 {0} v1{1}\n  right mesh index from {2} to {3} ", rightVector, currRightVector, lastRightCornerMeshIndex, i));
#endif
                lastRightCornerMeshIndex = i;
                rightVector = currRightVector;
            }
        }

        way.AddRange(CreatNewWay(meshPath, prevCornerMeshIndex, meshPath.Count - 1, new Vector(leftVector.from, end)));

        return way;
    }

    public List<WayVector> CreatNewWay(List<IMesh> meshPath, int startIndex, int endIndex, Vector vectorPath)
    {
        List<WayVector> way = new List<WayVector>();
        Vector3 from = vectorPath.from;
        Vector3 to;
#if MAP_DEBUG
        Debug.Log(string.Format("creat New Path {0}  st:{1} ed:{2}", vectorPath, startIndex, endIndex));
#endif
        for (int i = startIndex; i < endIndex; i++)
        {
            var edge = meshPath[i].GetAdjecantEdge(meshPath[i + 1]);
            to = vectorPath.GetCrossPoint(edge);
            if (from != to)
            {
                var v = new WayVector(from, to, meshPath[i]);
#if MAP_DEBUG
                Debug.Log(string.Format("addPath {0}  edge {1}", v, edge));
#endif
                way.Add(v);
            }
            from = to;
        }

        var v2 = new WayVector(from, vectorPath.to, meshPath[meshPath.Count - 1]);
        way.Add(v2);
#if MAP_DEBUG
        Debug.Log("addPath " + v2);
#endif
        return way;
    }

}
