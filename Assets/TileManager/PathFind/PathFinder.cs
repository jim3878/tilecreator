﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder
{

    List<MeshMap> mapList;
    AStar meshPathAlgo;

    public PathFinder(List<MeshMap> mapList)
    {
        this.mapList = mapList;
        meshPathAlgo = new AStar();
    }

    public List<WayVector> GetPath(Vector3 from, Vector3 to, float distance = 0)
    {
        Vector3 start, end;
        IMesh startMesh, endMesh;
        MeshMap map;
        //計算起終點及Mesh
        GetStartAndEndVector3(from, to, out start, out startMesh, out end, out endMesh, out map);
        
        if (startMesh == endMesh)
        {
            List<WayVector> simplaeWay = new List<WayVector>();
            simplaeWay.Add(new WayVector(start, end, startMesh));
            return simplaeWay;
        }

        List<IMesh> meshPath = meshPathAlgo.GetPath(startMesh, endMesh, map.meshList.ToArray());
        CornerPointFinder cornerFinder = new CornerPointFinder();

        List<WayVector> way = cornerFinder.Find(start, end, meshPath);


        while (distance > 0)
        {
            var vector = way[way.Count - 1];
            if (vector.Direction.magnitude < distance)
            {
                distance -= vector.Direction.magnitude;
                way.RemoveAt(way.Count - 1);
                
                if (way.Count == 0)
                {
                    List<WayVector> simplaeWay = new List<WayVector>();
                    simplaeWay.Add(new WayVector(start, start, startMesh));
                    return simplaeWay;
                }
            }
            else
            {
                vector.to = Vector3.MoveTowards(vector.from, vector.to, vector.Direction.magnitude - distance);
                break;
            }
        }

        return way;
    }

    private void GetStartAndEndVector3(Vector3 from, Vector3 to, out Vector3 start, out IMesh startMesh, out Vector3 end, out IMesh endMesh, out MeshMap map)
    {
        start = from;
        end = to;
        Vector3 pos;
        map = null;
        IMesh tempMesh;
        startMesh = default(IMesh);
        float distance;
        float minDist = float.MaxValue;
        foreach (var land in mapList)
        {
            if (land.IsPointIn(from))
            {
                //Debug.Log("player at map"+mapList.FindIndex(x => x == land));
                map = land;
                start = from;
                foreach (var m in land.meshList)
                {
                    if (m.IsPointIn(from))
                    {
                        startMesh = m;
                        break;
                    }
                }
                break;
            }
            else
            {
                pos = land.ClosedPoint(from, out tempMesh);
                distance = (pos - from).magnitude;
                //Debug.Log(string.Format("player is closed mesh{2} of map{0} with distance {1}", mapList.FindIndex(x => x == land),distance, tempMesh.Index));
                if (distance < minDist)
                {
                    minDist = distance;
                    startMesh = tempMesh;
                    map = land;
                    start = pos;
                }
            }
        }

        end = map.ClosedPoint(to, out endMesh);
    }
}
