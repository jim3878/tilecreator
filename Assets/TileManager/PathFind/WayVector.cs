﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayVector : Vector
{
    private IMesh mesh;
    public int GridID
    {
        get
        {
            return mesh.GridIndex;
        }
    }

    public WayVector(Vector3 from, Vector3 to,IMesh mesh) : base(from, to)
    {
        this.mesh = mesh;
    }
}
