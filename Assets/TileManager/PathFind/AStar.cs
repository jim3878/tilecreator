﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AStar
{

    public class MeshData
    {
        public IMesh mapMesh;
        public IMesh previous;
        private float goalDistance, startDistance;

        public float GoalDistance
        {
            get
            {
                return goalDistance;
            }
        }

        public float StartDistance
        {
            get
            {
                return startDistance;
            }
        }

        public float distance
        {
            get
            {
                return goalDistance + startDistance;
            }
        }
        public bool isDone;
        public int[] AllNeighbor
        {
            get
            {
                return mapMesh.GetAllAdjacent();
            }
        }

        public MeshData(IMesh mapMesh)
        {
            this.mapMesh = mapMesh;
            previous = null;
            goalDistance = startDistance = float.MaxValue;
            isDone = false;
        }

        public void SetGoalDistance(float distance)
        {
            this.goalDistance = distance;
        }

        public void SetStartDistance(float distance)
        {
            this.startDistance = distance;
        }

        public void SetPrevious(IMesh mapMesh)
        {
            this.previous = mapMesh;
        }

        public void Done()
        {
            isDone = true;
        }

        public override bool Equals(object obj)
        {
            if (obj is MeshData)
            {
                var d = (MeshData)obj;
                return d.mapMesh == this.mapMesh;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return mapMesh.GetHashCode();
        }
    }

    private MeshData GetMinDistanceMesh(Dictionary<IMesh, MeshData> openData)
    {
        float minDist = float.MaxValue;
        MeshData minMesh = null;
        int length = openData.Count;
        foreach (var data in openData)
        {
            if (data.Value.distance < minDist)
            {
                minDist = data.Value.distance;
                minMesh = data.Value;
            }
        }

        return minMesh;
    }

    public List<IMesh> GetPath(IMesh a, IMesh b, IMesh[] map)
    {
        Dictionary<IMesh, MeshData> openData = new Dictionary<IMesh, MeshData>();
        Dictionary<IMesh, MeshData> closedData = new Dictionary<IMesh, MeshData>();

        var currentMesh = new MeshData(a);
        openData.Add(a, currentMesh);
        currentMesh.SetStartDistance(0);
        currentMesh.SetGoalDistance(a.MoveCost(b));
        currentMesh.Done();
        //若終點週圍的節點皆計算過則跳出
        while (openData.Count() > 0)
        {
            //從等待貯列找出權重最小的網格
            currentMesh = GetMinDistanceMesh(openData);
            //currentMesh.Done();
            //將網格移至已計算的貯列
            openData.Remove(currentMesh.mapMesh);
            closedData.Add(currentMesh.mapMesh, currentMesh);
            //若最權重最小網格為終點則跳出
            if (currentMesh.mapMesh == b)
                break;

            //計算與當前相鄰網格者資料
            foreach (var adjacentIndex in currentMesh.AllNeighbor)
            {
                var adjacentMesh = map[adjacentIndex];
                var siteData = new MeshData(adjacentMesh);

                //跳過障礙物網格以及已估值網格
                if (!closedData.ContainsKey(adjacentMesh) && !adjacentMesh.IsBarrier)
                {
                    float currentDistance = currentMesh.StartDistance + currentMesh.mapMesh.MoveCost(siteData.mapMesh);
                    if (openData.ContainsKey(adjacentMesh))
                    {
                        //若相鄰網格在等待貯列中，則比較其權重是否更佳
                        siteData = openData[adjacentMesh];

                        if (siteData.StartDistance < currentDistance)
                        {
                            //填入數值
                            //Debug.Log(currentDistance);
                            siteData.SetStartDistance(currentDistance);//填入與起點距離
                            siteData.SetGoalDistance(siteData.mapMesh.MoveCost(b));//填入終點距離
                            siteData.SetPrevious(currentMesh.mapMesh);//設為當前網格子網格
                        }
                    }
                    else
                    {
                        //若相鄰網格不在貯列中，加入貯列並填入數值
                        openData.Add(adjacentMesh, siteData);
                        siteData.SetStartDistance(currentDistance);
                        siteData.SetGoalDistance(siteData.mapMesh.MoveCost(b));
                        siteData.SetPrevious(currentMesh.mapMesh);
                    }

                }
            }
        }

        List<IMesh> path = new List<IMesh>();
        var goalSite = closedData[b];
        return ReconstructPath(path,closedData,goalSite);
    }

    private List<IMesh> ReconstructPath(List<IMesh> path, IDictionary<IMesh, MeshData> closedData, MeshData b)
    {
        //由終點回朔至起點並畫為路徑
        path.Add(b.mapMesh);
        if (b.previous != null)
        {
            return  ReconstructPath(path, closedData, closedData[b.previous]);
        }else
        {
            path.Reverse();
            return path;
        }
    }
}
