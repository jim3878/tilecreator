﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GridTile
{
    public bool IsBarrier
    {
        get
        {
            return false;
        }
    }

    public int ID;

    public List<Vector3> Vertices
    {
        get
        {
            var mVertices = new List<Vector3>();

            mVertices.Add(GetPosition + new Vector3(gridSize.x * -0.5f, gridSize.y * 0.5f, 0));
            mVertices.Add(GetPosition + new Vector3(gridSize.x * 0.5f, gridSize.y * 0.5f, 0));
            mVertices.Add(GetPosition + new Vector3(gridSize.x * 0.5f, gridSize.y * -0.5f, 0));
            mVertices.Add(GetPosition + new Vector3(gridSize.x * -0.5f, gridSize.y * -0.5f, 0));

            return mVertices;
        }
    }

    private Vector3[] mVertices;

    private Vector2 gridSize = Vector2.one;

    public Vector3 GetPosition
    {
        get
        {
            return CoordinateToPosition(coordinate);
        }
    }

    public GridTile SetGridSize(Vector2 gridSize)
    {
        this.gridSize = gridSize;
        return this;
    }

    public Coordinate coordinate;

    public GridTile(Coordinate coordinate,int id)
    {
        this.ID = id;
        this.coordinate = coordinate;
    }

    private Vector3 CoordinateToPosition(Coordinate coordinate)
    {
        return new Vector3(coordinate.i * gridSize.x - gridSize.x / 2, coordinate.j * gridSize.y - gridSize.y / 2);
    }
}
