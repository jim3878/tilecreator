﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// 將traingle 轉成 triangleMesh
/// </summary>
public class TriangleMeshFactory  {

	public List<TriangleMesh> TriangleToTriangleMesh(List<Triangle> triangleList)
    {
        List<TriangleMesh> result = new List<TriangleMesh>();
        
        for(int i = 0; i < triangleList.Count; i++)
        {
            var tm = new TriangleMesh(triangleList[i], triangleList[i].ID);
            tm.Index = i;
            result.Add(tm);
        }

        bool isLink;
       
        foreach(var t1 in result)
        {
            foreach(var t2 in (from t in result where t!=t1 select t))
            {
                isLink = false;
                for(int i = 0; i < 3; i++)
                {
                    for(int j = 0; j < 3; j++)
                    {
                        if (t1.Vectors[i].EqualInLine(t2.Vectors[j]))
                        {
                            t1.LinkTriangle(t2);
                            isLink = true;
                            break;
                        }
                    }
                    if (isLink)
                        break;
                }
            }
        }

        return result;
    }
}
