﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Vector
{
    public Vector3 from;
    public Vector3 to;

    public Vector(Vector3 from, Vector3 to)
    {
        this.from = from;
        this.to = to;
    }

    public Vector3 Direction
    {
        get
        {
            return to - from;
        }
    }



    public bool IsPointTouch(Vector3 point)
    {
        if (point.x >= Mathf.Min(from.x, to.x) && point.x <= Mathf.Max(from.x, to.x) &&
            point.y >= Mathf.Min(from.y, to.y) && point.y <= Mathf.Max(from.y, to.y))
        {

            return Vector3.Cross(new Vector(from, point).Direction, Direction).magnitude == 0;
        }
        return false;
    }

    public Vector GetReverse()
    {
        return new Vector(to, from);
    }

    public Vector3 GetCrossPoint(Vector other)
    {
        var s = new Vector(from, other.from);
        var a = this;
        var b = other;

        if (from == other.from || from == other.to)
        {
            return from;
        }
        if (to == other.from || to == other.to)
        {
            return to;
        }

        return from + a.Direction * Vector3.Cross(s.Direction, b.Direction).z / Vector3.Cross(a.Direction, b.Direction).z;
    }

    public bool IsCross(Vector other)
    {
        var mCenter = Direction / 2 + this.from;
        var otherCenter = other.Direction / 2 + other.from;
        

        if (Mathf.Abs(mCenter.x - otherCenter.x) <= (Mathf.Abs(other.Direction.x) / 2 + Mathf.Abs(Direction.x) / 2) &&
            Mathf.Abs(mCenter.y - otherCenter.y) <= (Mathf.Abs(other.Direction.y) / 2 + Mathf.Abs(Direction.y) / 2))
        {

            if ((Vector3.Dot(Vector3.Cross(this.to - this.from, other.from - this.from),
                    Vector3.Cross(this.to - this.from, other.to - this.from)) <= 0
                && Vector3.Dot(Vector3.Cross(other.to - other.from, this.from - other.from),
                    Vector3.Cross(other.to - other.from, this.to - other.from)) <= 0))
            {
                return this.from != other.from && this.from != other.to &&
                    this.to != other.from && this.to != other.to;
            }
        }

        return false;
    }

    public override string ToString()
    {
        return string.Format("from: {0} to: {1}", from, to);
    }

    public bool EqualInLine(Vector other)
    {
        return (other.from == to && other.to == from) ||
            (from == other.from && to == other.to);
    }

    public override bool Equals(object obj)
    {
        if (obj is Vector)
        {
            var other = obj as Vector;
            return other.from == from && other.to == to;
        }
        return false;
    }

    public override int GetHashCode()
    {
        return from.GetHashCode() + to.GetHashCode();
    }
}
