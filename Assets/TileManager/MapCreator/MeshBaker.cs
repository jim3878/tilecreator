﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class Circle
{
    public Vector3 center;
    public float radius;
    public Circle(Vector3 center, float radius)
    {
        this.center = center;
        this.radius = radius;
    }
}

[Serializable]
//按順時針編成
public class Triangle
{
    [SerializeField]
    private List<Vector3> point = new List<Vector3>();
    public int ID;
    public List<Vector3> Vertices
    {
        get
        {
            return point;
        }
    }

    public List<Vector> Vectors
    {
        get
        {
            List<Vector> e = new List<Vector>();
            for (int i = 0; i < 2; i++)
            {
                e.Add(new Vector(point[i], point[i + 1]));
            }
            e.Add(new Vector(point[2], point[0]));
            return e;
        }
    }

    public bool IsPointIn(Vector3 point)
    {
        return Mathf.Sign(Vector3.Cross(Vectors[0].Direction, point - Vertices[0]).z) == Mathf.Sign(Vector3.Cross(Vectors[0].Direction, Vertices[2] - Vertices[0]).z) &&
            Mathf.Sign(Vector3.Cross(Vectors[1].Direction, point - Vertices[1]).z) == Mathf.Sign(Vector3.Cross(Vectors[1].Direction, Vertices[0] - Vertices[1]).z) &&
            Mathf.Sign(Vector3.Cross(Vectors[2].Direction, point - Vertices[2]).z) == Mathf.Sign(Vector3.Cross(Vectors[2].Direction, Vertices[1] - Vertices[2]).z);
    }

    public Triangle(Vector3 p1, Vector3 p2, Vector3 p3, int id = -1)
    {
        this.ID = id;
        point.Add(p1);
        point.Add(p2);
        point.Add(p3);
    }

    public override string ToString()
    {
        return string.Format("p1{0} p2{1} p3{2}", point[0], point[1], point[2]);
    }

    public bool IsAdjacent(Triangle other)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (this.Vectors[i].EqualInLine(other.Vectors[j]))
                {
                    return true;
                }
            }
        }
        return false;
    }
}

public class MeshBaker
{
    private float EP = 0.0001f;

    public List<Vector> lineList = new List<Vector>();
    public List<Vector3> points = new List<Vector3>();
    public List<Vector> edges = new List<Vector>();
    private List<Vector> unusedEdges = new List<Vector>();
    private int meshID;

    public MeshBaker(List<Vector3> points, List<Vector> edges, int id)
    {
        this.meshID = id;
        this.points = points;
        this.edges = edges;
        unusedEdges.AddRange(edges);
        //Debug.Log("bake start");
        //foreach(var edge in edges)
        //{
        //    Debug.Log(edge);
        //}
    }

    public List<Vector3> GetVisialbePoint(Vector v)
    {
        bool isCross = false;
        List<Vector3> visuable = new List<Vector3>();
        foreach (var p in points)
        {
            Vector vp = new Vector(v.from, p);
            if (Vector3.Cross(v.Direction, vp.Direction).z >= 0)
            {
                //在向量的逆時鐘方向故剔除
                continue;
            }
            isCross = false;
            foreach (var e in edges)
            {
                if (e == v)
                {
                    continue;
                }
                //是否與其它邊界交錯
                if (e.IsCross(vp))
                {
                    isCross = true;
                    break;
                }
                if (e.IsCross(new Vector(v.to, p)))
                {
                    isCross = true;
                    break;
                }
            }

            foreach (var e in lineList)
            {
                //是否與其它約束邊交錯
                if (e.IsCross(vp))
                {
                    isCross = true;
                    break;
                }
                if (e.IsCross(new Vector(v.to, p)))
                {
                    isCross = true;
                    break;
                }
            }
            if (!isCross)
            {
                visuable.Add(p);
            }
        }
        return visuable;
    }

    public List<List<Triangle>> CreatDelaunayMap()
    {
        List<List<Triangle>> map = new List<List<Triangle>>();
        int rc = 0;
        while (unusedEdges.Count > 0 && rc < 100)
        {
            rc++;
            //Debug.Log("第 " + rc+ " 次創建三角形");
            map.Add(CreatDelaunay());
        }
        return map;
    }

    public List<Triangle> CreatDelaunay()
    {
        //任取一條邊界
        //var emptyEdges=from edge in edges where 
        lineList = new List<Vector>();
        lineList.Add(unusedEdges[0]);
        unusedEdges.RemoveAt(0);
        List<Triangle> triangleList = new List<Triangle>();
        int count = 0;
        do
        {
            count++;
            //從線堆中取出一條線
            var e = lineList[0];
            //Debug.Log("取出 " + e);
            lineList.RemoveAt(0);

            //找出該線的DT點
            var p3 = GetDT(e);
            //Debug.Log(string.Format( "Get DT {0} for {1}", p3,e));
            if (p3 == default(Vector3))
            {
                //Debug.Log("找不到DT點");
                continue;
            }
            //產生三角形
            var triangle = new Triangle(e.from, e.to, p3, this.meshID);
            triangleList.Add(triangle);
           // Debug.Log("創建 " + triangle);

            int edgesIndex = edges.FindIndex(x => x.EqualInLine(triangle.Vectors[1]));
            if (edgesIndex == -1)
            {
                int findIndex = lineList.FindIndex(x => x.EqualInLine(triangle.Vectors[1]));
                if (findIndex != -1)
                {
                    //Debug.Log("remove " + triangle.Vectors[1]);
                    lineList.RemoveAt(findIndex);
                }
                else
                {
                    //Debug.Log("add " + triangle.Vectors[1]);
                    lineList.Add(triangle.Vectors[1].GetReverse());
                }
            }
            else
            {
                unusedEdges.Remove(edges[edgesIndex]);
            }

            edgesIndex = edges.FindIndex(x => x.EqualInLine(triangle.Vectors[2]));
            if (edgesIndex == -1)
            {
                int findIndex = lineList.FindIndex(x => x.EqualInLine(triangle.Vectors[2]));
                if (findIndex != -1)
                {
                    //Debug.Log("remove " + triangle.Vectors[2]);
                    lineList.RemoveAt(findIndex);
                }
                else
                {
                    //Debug.Log("add " + triangle.Vectors[2]);
                    lineList.Add(triangle.Vectors[2].GetReverse());
                }
            }
            else
            {
                unusedEdges.Remove(edges[edgesIndex]);
            }

        } while (lineList.Count > 0);
        //Debug.Log("LineCount " + lineList.Count);
        //Debug.Log("count "+count);
        return triangleList;

    }

    public Vector3 GetDT(Vector v)
    {
        var visiblePoint = GetVisialbePoint(v);
        //Debug.Log("可視點數量" + visiblePoint.Count);
        if (visiblePoint.Count == 0)
        {
            return default(Vector3);
        }

        Vector3 dt = visiblePoint[0];
        

        bool isFound = true;
        float smallAngle = Vector3.Angle(v.from - dt, v.to - dt);
        float angle;

        while (isFound)
        {
            isFound = false;
            Circle c = GetCircleBox(v.from, v.to, dt);
            foreach (var p in visiblePoint)
            {
                if ((p - c.center).magnitude >= c.radius)
                {
                    //不在包圍網中
                    continue;
                }
                angle = Vector3.Angle(v.from - p, v.to - p);
                if (angle > smallAngle)
                {
                    smallAngle = angle;
                    dt = p;
                    isFound = true;
                    break;
                }
            }
        }

        return dt;
    }

    public Circle GetCircleBox(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        Vector3 q = new Vector3();
        float r = 0;
        float x12 = p2.x - p1.x;
        float y12 = p2.y - p1.y;
        float x13 = p3.x - p1.x;
        float y13 = p3.y - p1.y;
        float z2 = x12 * (p1.x + p2.x) + y12 * (p1.y + p2.y);
        float z3 = x13 * (p1.x + p3.x) + y13 * (p1.y + p3.y);
        float d = 2.0f * (x12 * (p3.y - p2.y) - y12 * (p3.x - p2.x));
        if (Mathf.Abs(d) < EP)                           //共线，圆不存在
            return null;
        q.x = (y13 * z2 - y12 * z3) / d;
        q.y = (x12 * z3 - x13 * z2) / d;
        r = (p1 - q).magnitude;
        return new Circle(q, r);
    }
}
