﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class MapData 
{
    public int ID;
    public string name = "map";
    public GridMap gridMap = new GridMap();
    public List<MeshMap> meshMapList;

    public MapData(int id)
    {
        this.ID = id;
        gridMap = new GridMap();
        meshMapList = new List<MeshMap>();
        
    }

}

[System.Serializable]
public class MapTable : ScriptableObject
{
    [HideInInspector]
    public int currentIndex = 0;
    public List<GridData> gridDataList;
    public List<MapData> mapList;

    public MapTable()
    {
        gridDataList = new List<GridData>();
        gridDataList.Add(new GridData(0, new Color(0, 1, 0, 0.2f)));
    }

    public void UpdateGridDataOfMesh()
    {
        foreach(var map in mapList)
        {
            foreach(var meshMap in map.meshMapList)
            {
                foreach(var mesh in meshMap.meshList)
                {
                    mesh.moveCost = gridDataList[mesh.GridIndex].moveCost;
                }
            }
        }
    }
}

[System.Serializable]
public class GridData
{
    public int ID;
    public string name = "tiles";
    public Color color;
    public bool isBarrer;
    public float moveCost;

    public GridData(int id,Color color)
    {
        this.ID = id;
        this.color = color;
        this.isBarrer = false;
    }
}