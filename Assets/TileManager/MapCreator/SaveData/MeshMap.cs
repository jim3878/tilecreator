﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class MeshMap
{
    //public List<MeshLand> meshLandList = new List<MeshLand>();
    public List<TriangleMesh> meshList = new List<TriangleMesh>();
    public MeshMap(List<TriangleMesh> meshList)
    {
        this.meshList = meshList;
    }

    public static List<MeshMap> Creat(GridMap gridMap, List<GridData> gridData)
    {
        List<MeshMap> mpList = new List<MeshMap>();
        List<List<Triangle>> triangleMapList = new List<List<Triangle>>();
        foreach (var data in gridData)
        {
            List<PolygonVector> pvList = GridMapToPolygonVector(gridMap, gridData, data.ID);
            if (pvList == null)
                continue;
            foreach (var pv in pvList)
            {
                triangleMapList.AddRange(PolygonVectorToTriangleList(pv, data.ID));
            }
        }
        triangleMapList = TriangleMapMerge(triangleMapList);
        mpList = TriangleMapListToMeshMapList(triangleMapList);

        return mpList;
    }

    static List<MeshMap> TriangleMapListToMeshMapList(List<List<Triangle>> triangleMapList)
    {
        List<MeshMap> result = new List<MeshMap>();
        TriangleMeshFactory factory = new TriangleMeshFactory();

        foreach (var triangleMap in triangleMapList)
        {

            result.Add(new MeshMap(factory.TriangleToTriangleMesh(triangleMap)));
        }
        Debug.Log("MeshMapCount " + result.Count);
        return result;
    }

    static List<PolygonVector> GridMapToPolygonVector(GridMap gridMap, List<GridData> gridData, int id)
    {
        List<PolygonVector> pvList = new List<PolygonVector>();
        var pv = new PolygonVector();
        Vector3[] ignor = GetIgnorPoint(gridMap, gridData).ToArray();
        string debugS = "";
        foreach (var tile in gridMap.TileList.FindAll(x => x.ID == id))
        {
            var Left = gridMap.GetAdjacent(GridMap.AdjacentEnum.LEFT, tile);
            var right = gridMap.GetAdjacent(GridMap.AdjacentEnum.RIGHT, tile);
            var lower = gridMap.GetAdjacent(GridMap.AdjacentEnum.LOWER, tile);
            var upper = gridMap.GetAdjacent(GridMap.AdjacentEnum.UPPER, tile);
            var lowerLeft = gridMap.GetAdjacent(GridMap.AdjacentEnum.LOWER_LEFT, tile);
            var lowerRight = gridMap.GetAdjacent(GridMap.AdjacentEnum.LOWER_RIGHT, tile);
            var upperLeft = gridMap.GetAdjacent(GridMap.AdjacentEnum.UPPER_LEFT, tile);
            var upperRight = gridMap.GetAdjacent(GridMap.AdjacentEnum.UPPER_RIGHT, tile);

            float offsetX = gridMap.GridSize.x * 0.49f;
            float offsetY = gridMap.GridSize.y * 0.49f;
            
            int leftEdge = -1;
            int rightEdge = -1;
            int upperEdge = -1;
            int lowerEdge = -1;
            if (Left == null || Left.ID != id)
            {
                debugS += "add left edge  " + tile.Vertices[3] + "  " + tile.Vertices[0] + "\n";
                leftEdge = pv.AddEdge(tile.Vertices[3], tile.Vertices[0]);
            }
            if (upper == null || upper.ID != id)
            {
                debugS += "add upper edge  " + tile.Vertices[0] + "  " + tile.Vertices[1] + "\n";
                upperEdge = pv.AddEdge(tile.Vertices[0], tile.Vertices[1]);
            }
            if (right == null || right.ID != id)
            {
                debugS += "add right edge  " + tile.Vertices[1] + "  " + tile.Vertices[2] + "\n";
                rightEdge = pv.AddEdge(tile.Vertices[1], tile.Vertices[2]);
            }
            if (lower == null || lower.ID != id)
            {
                debugS += "add lower edge  " + tile.Vertices[2] + "  " + tile.Vertices[3] + "\n";
                lowerEdge = pv.AddEdge(tile.Vertices[2], tile.Vertices[3]);
            }

            if (Left == null)
            {
                pv.MoveEdge(leftEdge, new Vector3(offsetX, 0));
            }
            if (right == null)
            {
                pv.MoveEdge(rightEdge, new Vector3(-offsetX, 0));
            }
            if (upper == null)
            {
                pv.MoveEdge(upperEdge, new Vector3(0, -offsetY));
            }
            if (lower == null)
            {
                pv.MoveEdge(lowerEdge, new Vector3(0, offsetY));
            }

            if (upperLeft == null && Left != null && upper != null)
            {
                Vector3 p0 = tile.Vertices[0] + new Vector3(0, -offsetY);
                Vector3 p1 = tile.Vertices[0] + new Vector3(offsetX, -offsetY);
                Vector3 p2 = tile.Vertices[0] + new Vector3(offsetX, 0);

                var v0 = new Vector(p0, p1);
                var v1 = new Vector(p1, p2);
                pv.InsertEdge(tile.Vertices[0], v0, v1);
            }

            if (upperRight == null && right != null && upper != null)
            {
                Vector3 p0 = tile.Vertices[1] + new Vector3(-offsetX, 0);
                Vector3 p1 = tile.Vertices[1] + new Vector3(-offsetX, -offsetY);
                Vector3 p2 = tile.Vertices[1] + new Vector3(0, -offsetY);

                var v0 = new Vector(p0, p1);
                var v1 = new Vector(p1, p2);
                pv.InsertEdge(tile.Vertices[1], v0, v1);
            }

            if (lowerRight == null && right != null && lower != null)
            {

                Vector3 p0 = tile.Vertices[2] + new Vector3(0, offsetY);
                Vector3 p1 = tile.Vertices[2] + new Vector3(-offsetX, offsetY);
                Vector3 p2 = tile.Vertices[2] + new Vector3(-offsetX, 0);

                var v0 = new Vector(p0, p1);
                var v1 = new Vector(p1, p2);
                pv.InsertEdge(tile.Vertices[2], v0, v1);
            }
            if (lowerLeft == null && lower != null && Left != null)
            {

                Vector3 p0 = tile.Vertices[3] + new Vector3(offsetX, 0);
                Vector3 p1 = tile.Vertices[3] + new Vector3(offsetX, offsetY);
                Vector3 p2 = tile.Vertices[3] + new Vector3(0, offsetY);

                var v0 = new Vector(p0, p1);
                var v1 = new Vector(p1, p2);
                pv.InsertEdge(tile.Vertices[3], v0, v1);
            }
        }
        if (pv.EdgeList.Count == 0)
            return null;
        //Debug.Log(debugS);
        //Debug.Log("優化" + id);
        pv.Optimize(ignor);
        Debug.Log(pv);
        pvList.Add(pv);

        return pvList;
    }

    static List<List<Triangle>> PolygonVectorToTriangleList(PolygonVector pv, int id)
    {
        MeshBaker baker = new MeshBaker(pv.VerticeList, pv.EdgeList, id);
        var simpleMapList = baker.CreatDelaunayMap();

        return simpleMapList;
    }


    /// <summary>
    /// 不可被消除的關鍵節點
    /// </summary>
    /// <param name="gridMap"></param>
    /// <param name="gridData"></param>
    /// <returns></returns>
    static List<Vector3> GetIgnorPoint(GridMap gridMap, List<GridData> gridData)
    {
        List<Vector3> ignor = new List<Vector3>();
        foreach (var data in gridData)
        {
            foreach (var tile in gridMap.TileList.FindAll(x => x.ID == data.ID))
            {
                var upper = gridMap.GetAdjacent(GridMap.AdjacentEnum.UPPER, tile);
                var lower = gridMap.GetAdjacent(GridMap.AdjacentEnum.LOWER, tile);
                var left = gridMap.GetAdjacent(GridMap.AdjacentEnum.LEFT, tile);
                var right = gridMap.GetAdjacent(GridMap.AdjacentEnum.RIGHT, tile);
                var upperLeft = gridMap.GetAdjacent(GridMap.AdjacentEnum.UPPER_LEFT, tile);
                var upperRight = gridMap.GetAdjacent(GridMap.AdjacentEnum.UPPER_RIGHT, tile);
                var lowerRight = gridMap.GetAdjacent(GridMap.AdjacentEnum.LOWER_RIGHT, tile);
                var lowerLeft = gridMap.GetAdjacent(GridMap.AdjacentEnum.LOWER_LEFT, tile);
                if (upperLeft != null && upperLeft.ID != data.ID && (upper == null || left == null))
                {
                    ignor.Add(tile.Vertices[0]);
                }
                if (upperRight != null && upperRight.ID != data.ID && (upper == null || right == null))
                {
                    ignor.Add(tile.Vertices[1]);
                }
                if (lowerRight != null && lowerRight.ID != data.ID && (lower == null || right == null))
                {
                    ignor.Add(tile.Vertices[2]);
                }
                if (lowerLeft != null && lowerLeft.ID != data.ID && (lower == null || left == null))
                {
                    ignor.Add(tile.Vertices[3]);
                }
            }
        }
        return ignor;
    }

    /// <summary>
    /// 將不同編號但相連的三角形集合併
    /// </summary>
    /// <param name="mapList"></param>
    /// <returns></returns>
    private static List<List<Triangle>> TriangleMapMerge(List<List<Triangle>> mapList)
    {
        List<List<Triangle>> result = new List<List<Triangle>>();
        List<bool> isMerge = new List<bool>();
        for (int i = 0; i < mapList.Count; i++)
        {
            isMerge.Add(false);
        }

        for (int i = 0; i < mapList.Count; i++)
        {
            var map = mapList[i];
            if (!isMerge[i])
            {
                isMerge[i] = true;
                var currentList = new List<Triangle>();
                currentList.AddRange(map);
                for (int j = i + 1; j < mapList.Count; j++)
                {
                    var other = mapList[j];
                    if (!isMerge[j] && IsMapAdjacent(currentList, other))
                    {
                        Debug.Log(string.Format("merge {0} to {1}",j,i));
                        isMerge[j] = true;
                        currentList.AddRange(other);
                        j = i;
                    }
                }
                Debug.Log(string.Format("save {0}", i));
                result.Add(currentList);
            }
        }

        return result;
    }

    private static bool IsMapAdjacent(List<Triangle> a, List<Triangle> b)
    {
        foreach (var t1 in a)
        {
            foreach (var t2 in b)
            {
                if (t1.IsAdjacent(t2))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public bool IsPointIn(Vector3 point)
    {
        foreach (var m in meshList)
        {
            if (m.IsPointIn(point))
            {
                return true;
            }
        }
        return false;
    }

    public Vector3 ClosedPoint(Vector3 point, out IMesh targetMesh)
    {
        float minDistance = float.MaxValue;
        Vector3 minPoint = default(Vector3);
        Vector3 currentPoint;
        float currentDistance;
        targetMesh = null;
        foreach (var m in meshList)
        {
            currentDistance = m.Distance(point, out currentPoint);
            if (currentDistance == -1)
            {
                targetMesh = m;
                return point;
            }
            else if (currentDistance < minDistance)
            {
                targetMesh = m;
                minPoint = currentPoint;
                minDistance = currentDistance;
            }

        }
        if (targetMesh == null)
        {
            throw new System.Exception("targetMesh cant be null.");
        }
        return minPoint;
    }

    public static MeshMap[] Convert(GridMap gridMap)
    {
        //判斷順逆時針
        //var vectorGridMap = gridMap.VectorPolygonArray;
        //VectorPolygon.SetClockWise(vectorGridMap);

        //List<VectorPolygon> land = new List<VectorPolygon>();
        //List<VectorPolygon> hole = new List<VectorPolygon>();
        ////切分
        //foreach(var polygon in vectorGridMap)
        //{
        //    if (polygon.IsClockWise)
        //        land.Add(polygon);
        //    else
        //        hole.Add(polygon);
        //}

        //List<Vector3> verticesList = new List<Vector3>();
        //List<Vector> edgeList = new List<Vector>();
        //foreach (var polygon in vectorGridMap)
        //{
        //    polygon.Optimize();
        //    verticesList.AddRange(polygon.vertices);
        //    edgeList.AddRange(polygon.Vectors);
        //}

        //MeshBaker baker = new MeshBaker(verticesList, edgeList);

        //var triangle = baker.CreatDelaunay();
        //TriangleMeshFactory triangleMeshFactory = new TriangleMeshFactory();
        //gridMap.mesheArray = triangleMeshFactory.TriangleToTriangleMesh(triangle);
        return null;
    }
}


