﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class GridMap
{
    public enum AdjacentEnum
    {
        LEFT,
        UPPER_LEFT,
        UPPER,
        UPPER_RIGHT,
        RIGHT,
        LOWER_RIGHT,
        LOWER,
        LOWER_LEFT
    }
    
    [SerializeField]
    private List<GridTile> tileList = new List<GridTile>();
    public List<GridTile> TileList
    {
        get
        {
            foreach (var t in tileList)
            {
                t.SetGridSize(gridSize);
            }
            return tileList;
        }
        set
        {
            tileList = value;
        }
    }


    //private IPolygonSerilizer polygonSerilizer = new TurnRightCutter();
    //private TriangleMeshFactory triangleMeshFactory = new TriangleMeshFactory();
    //public TriangleMesh[] mesheArray;
    [SerializeField]
    private Vector2 gridSize = Vector2.one;
    public GridMap()
    {
        //GridColorList.Add(new Color(0, 1, 0, 0.2f));
    }

    public void AddTile(GridTile tile)
    {
        tile.SetGridSize(gridSize);
        TileList.Add(tile);
    }

    public Vector2 GridSize
    {
        set
        {
            gridSize = value;
            foreach (var t in tileList)
            {
                t.SetGridSize(gridSize);
            }
        }
        get
        {
            return gridSize;
        }
    }
    
    public GridTile GetAdjacent(AdjacentEnum adjacent, GridTile tile)
    {
        switch (adjacent)
        {
            case AdjacentEnum.LEFT:
                return tileList.Find((x) => x.coordinate.Equals(new Coordinate(tile.coordinate.i - 1, tile.coordinate.j)));
            case AdjacentEnum.UPPER_LEFT:
                return tileList.Find((x) => x.coordinate.Equals(new Coordinate(tile.coordinate.i - 1, tile.coordinate.j + 1)));
            case AdjacentEnum.UPPER:
                return tileList.Find((x) => x.coordinate.Equals(new Coordinate(tile.coordinate.i, tile.coordinate.j + 1)));
            case AdjacentEnum.UPPER_RIGHT:
                return tileList.Find((x) => x.coordinate.Equals(new Coordinate(tile.coordinate.i + 1, tile.coordinate.j + 1)));
            case AdjacentEnum.RIGHT:
                return tileList.Find((x) => x.coordinate.Equals(new Coordinate(tile.coordinate.i + 1, tile.coordinate.j)));
            case AdjacentEnum.LOWER_RIGHT:
                return tileList.Find((x) => x.coordinate.Equals(new Coordinate(tile.coordinate.i + 1, tile.coordinate.j - 1)));
            case AdjacentEnum.LOWER:
                return tileList.Find((x) => x.coordinate.Equals(new Coordinate(tile.coordinate.i, tile.coordinate.j - 1)));
            case AdjacentEnum.LOWER_LEFT:
                return tileList.Find((x) => x.coordinate.Equals(new Coordinate(tile.coordinate.i - 1, tile.coordinate.j - 1)));
            default:
                return null;
        }
    }
}
