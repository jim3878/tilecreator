﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TriangleMesh : IMesh
{
    [SerializeField]
    private int gridIndex;
    public int GridIndex
    {
        get
        {
            return gridIndex;
        }
    }
    public bool IsBarrier
    {
        get
        {
            return false;
        }
    }
    public int Index { get; set; }
    public float moveCost = 1;
    public Triangle triangle;
    [SerializeField]
    private List<int> adjacentTriangle = new List<int>();
    public Vector3 Position
    {
        get
        {
            Vector3 center = Vector3.zero;
            try
            {
                foreach (var p in triangle.Vertices)
                {
                    center += p;
                }
                return center / 3;
            }
            catch (Exception)
            {
                Debug.Log(triangle);
                throw new Exception();
            }
        }
    }
    public List<Vector> Vectors
    {
        get
        {
            return triangle.Vectors;
        }
    }
    public List<Vector3> Vertices
    {
        get
        {
            return triangle.Vertices;
        }
    }

    public TriangleMesh( Triangle triangle,int gridID)
    {

        this.triangle = triangle;
        this.gridIndex = gridID;
    }

    public bool IsPointIn(Vector3 point)
    {
        return triangle.IsPointIn(point);
    }

    public void LinkTriangle(TriangleMesh tMesh)
    {
        tMesh.AddAdjacentTriangle(this.Index);
        this.AddAdjacentTriangle(tMesh.Index);
    }

    public float Distance(Vector3 point)
    {
        Vector3 v;
        return Distance(point, out v);
    }

    public float Distance(Vector3 point,out Vector3 closedPoint)
    {
        float minDistance = float.MaxValue;
        float distance;
        float t;
        Vector3 tempPoint;
        Vector3 tempVector;
        closedPoint = Vector3.zero;
        foreach (var e in triangle.Vectors)
        {
            tempVector = point - e.from;
            if (Vector3.Cross(e.Direction, tempVector).z > 0)
            {
                t = Vector3.Dot(e.Direction, tempVector) / (e.Direction.magnitude* e.Direction.magnitude);
                if (t < 0)
                {
                    tempPoint = e.from;
                }
                else if (t > 1)
                {
                    tempPoint = e.to;
                }
                else
                {
                    tempPoint = e.from + e.Direction * t;
                }
                distance = (tempPoint - point).magnitude;

                if (distance < minDistance)
                {
                    minDistance = distance;
                    closedPoint = tempPoint;
                }
            }
        }
        if (minDistance == float.MaxValue)
        {
            return -1;
        }
        return minDistance;
    }

    private void AddAdjacentTriangle(int triangleIndex)
    {
        if (adjacentTriangle.Count > 3)
        {
            String tringleList = "";
            for(int i = 0; i < adjacentTriangle.Count; i++)
            {
                tringleList += adjacentTriangle[i] + "\n";
            }
            tringleList += triangleIndex + "\n";
            throw new Exception("只能有三個相鄰三角形 "+this+"\n"+tringleList);
        }
        else
        {
            if (!adjacentTriangle.Contains(triangleIndex))
                adjacentTriangle.Add(triangleIndex);
        }
    }

    public int[] GetAllAdjacent()
    {
        return adjacentTriangle.ToArray();
    }

    public float MoveCost(IMesh other)
    {
        if (other is TriangleMesh)
        {
            var t = other as TriangleMesh;

            return (t.Position - Position).magnitude * (moveCost + t.moveCost) / 2;
        }
        else
        {
            throw new Exception("Unknow Type " + other);
        }
    }

    public override string ToString()
    {
        return triangle.ToString();
    }

    public override int GetHashCode()
    {
        return triangle.GetHashCode();
    }

    public override bool Equals(object obj)
    {
        if (obj is TriangleMesh)
        {
            var t = obj as TriangleMesh;
            return t.triangle.Equals(this.triangle);
        }
        return false;
    }

    public Vector GetAdjecantEdge(IMesh mesh)
    {
        if (mesh is TriangleMesh)
        {
            var other = mesh as TriangleMesh;
            foreach (var t in other.triangle.Vectors)
            {
                var v = this.Vectors.Find(x => x.EqualInLine(t));
                if (v != null) return v;
            }
        }
        return null;
    }
}
