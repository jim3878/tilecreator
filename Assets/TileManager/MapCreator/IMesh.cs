﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMesh  {

	bool IsBarrier { get; }
    int[] GetAllAdjacent();
    float MoveCost(IMesh other);
    Vector GetAdjecantEdge(IMesh mesh);
    int Index { get; }
    
    int GridIndex { get; }
}
