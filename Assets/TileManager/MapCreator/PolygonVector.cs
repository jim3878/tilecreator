﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PolygonVector
{

    private List<Vector> edgeList = new List<Vector>();
    public List<Vector> EdgeList
    {
        get
        {
            return edgeList;
        }
    }

    public void MoveVertice(Vector3 vertice,Vector3 offset)
    {
        int index = edgeList.FindIndex(x => x.to == vertice);
        if (index != -1)
        {
            edgeList[index].to += offset;
        }
        index = edgeList.FindIndex(x => x.from == vertice);
        if (index != -1)
        {
            edgeList[index].to += offset;
        } 
    }

    public void MoveEdge(int edgeIndex, Vector3 direction)
    {
        if (edgeIndex < edgeList.Count)
        {
            var edge = edgeList[edgeIndex];
            Vector3 to = edge.to;
            Vector3 from = edge.from;

            edge.from += direction;
            edge.to += direction;

            int index = edgeList.FindIndex(x => x.to == from);
            if (index != -1)
            {
                edgeList[index].to = edge.from;
            }
            index = edgeList.FindIndex(x => x.from == to);
            if (index != -1)
            {
                edgeList[index].from = edge.to;
            }
        }
    }

    public List<Vector3> VerticeList
    {
        get
        {
            List<Vector3> result = new List<Vector3>();
            foreach (var v in edgeList)
            {
                if (result.FindIndex(x => x == v.from) == -1)
                {
                    result.Add(v.from);
                }
                if (result.FindIndex(x => x == v.to) == -1)
                {
                    result.Add(v.to);
                }
            }
            return result;
        }
    }

    public void InsertEdge(Vector3 vertice,params Vector[] arr)
    {
        int indexStart = edgeList.FindIndex(x => x.to == vertice);
        if (indexStart != -1)
        {
            edgeList[indexStart].to = arr[0].from;
        }
        int indexEnd = edgeList.FindIndex(x => x.from == vertice);
        if (indexEnd != -1)
        {
            edgeList[indexEnd].from = arr[arr.Length - 1].to;
        }

        foreach(var v in arr)
        {
            AddEdge(v);
        }
    }

    public int AddEdge(Vector3 from, Vector3 to)
    {
        Vector v = new Vector(from, to);
        return  AddEdge(v);
    }

    public int AddEdge(Vector v)
    {
        int n = edgeList.Count;
        edgeList.Add(v);
        return n;
    }

    //尋找邊界拐角點
    private int FindCornerIndex()
    {
        for (int i = 0; i < edgeList.Count; i++)
        {
            var nextEdge = edgeList.Find(x => x.to == edgeList[i].from);
            if (nextEdge != null && Vector3.Dot(edgeList[i].Direction.normalized, nextEdge.Direction.normalized) != 1)
            {
                //Debug.Log("corner " + edgeList[i]);
                return i;
            }
        }
        for (int i = 0; i < edgeList.Count; i++)
        {
            Debug.LogError(edgeList[i]);
        }
        throw new System.Exception("error in PolygonVector.FindCorner ");
    }

    public void SortEdge()
    {
        List<Vector> result = new List<Vector>();

        var cornerIndex = FindCornerIndex();
        result.Add(edgeList[cornerIndex]);
        Vector current = edgeList[cornerIndex];
        edgeList.RemoveAt(cornerIndex);
        while (edgeList.Count > 0)
        {
            int index = edgeList.FindIndex(x => x.from == current.to);
            if (index != -1)
            {
                result.Add(edgeList[index]);
                current = edgeList[index];
                edgeList.RemoveAt(index);
            }
            else
            {
                cornerIndex = FindCornerIndex();
                result.Add(edgeList[cornerIndex]);
                current = edgeList[cornerIndex];
                edgeList.RemoveAt(cornerIndex);
            }
        }
        edgeList = result;
    }

    public void Optimize(params Vector3[] ignor)
    {
        List<Vector> result = new List<Vector>();
        SortEdge();
        Vector3 startPoint = edgeList[0].from;
        for (int i = 0; i < edgeList.Count - 1; i++)
        {
            if (ignor.ToList().Contains(edgeList[i].to))
            {
                //Debug.Log("創建(iginor) " + new Vector(startPoint, edgeList[i].to));
                result.Add(new Vector(startPoint, edgeList[i].to));

                if (edgeList[i].to == edgeList[i + 1].from)
                {
                    startPoint = edgeList[i].to;
                }
                else
                {
                    startPoint = edgeList[i + 1].from;
                }
                continue;
            }
            //兩邊是否頭尾相連
            if (edgeList[i + 1].from == edgeList[i].to)
            {
                //兩邊是否為轉折
                if (Vector3.Dot(edgeList[i + 1].Direction.normalized, edgeList[i].Direction.normalized) != 1)
                {
                    result.Add(new Vector(startPoint, edgeList[i].to));
                    //Debug.Log("創建 " + new Vector(startPoint, edgeList[i].to));
                    startPoint = edgeList[i].to;
                }
            }
            else
            {
                //Debug.Log("創建 " + new Vector(startPoint, edgeList[i].to));
                result.Add(new Vector(startPoint, edgeList[i].to));
                startPoint = edgeList[i + 1].from;
            }
        }

        //Debug.Log("創建 " + new Vector(startPoint, edgeList[edgeList.Count - 1].to));
        result.Add(new Vector(startPoint, edgeList[edgeList.Count - 1].to));
        edgeList = result;
    }

    public override string ToString()
    {
        string s="";
        foreach(var edge in edgeList)
        {
            s += edge.ToString()+"\n";
        }
        return s;
    }
}
